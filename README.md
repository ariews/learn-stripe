# Stripe PlayBook

based on: https://github.com/xmeltrut/book-shop-php

```text
$ composer install
$ symfony server:ca:install
$ symfony server:start
```

## ngrok

```text
$ ngrok http https://localhost:8000
```

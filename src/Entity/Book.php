<?php

declare(strict_types=1);

namespace App\Entity;

use JsonSerializable;

class Book implements JsonSerializable
{
    protected string $title;

    protected float $price;

    protected string $description;

    protected int $id;

    public function __construct(int $id, string $title, float $price, string $description)
    {
        $this->id = $id;
        $this->title = $title;
        $this->price = $price;
        $this->description = $description;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'price' => $this->price,
        ];
    }
}

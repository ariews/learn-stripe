<?php

declare(strict_types=1);

namespace App\Helper;

use App\Entity\Book;

class BookHelper
{
    protected static array $books = [
        [1, 'Product 1', 13.5],
        [2, 'Product 2', 15],
        [3, 'Product 3', 17],
        [4, 'Product 4', 19.9],
    ];

    public static function createBooks(): array
    {
        return array_map(function (array $book) {
            return self::toEntity($book);
        }, self::$books);
    }

    public static function findById(int $id): ?Book
    {
        foreach (self::$books as $book) {
            if ($id === $book[0]) {
                return self::toEntity($book);
            }
        }

        return null;
    }

    private static function toEntity(array $book): Book
    {
        [$id, $name, $price] = $book;

        return new Book($id, $name, $price, 'Book description');
    }
}

<?php

declare(strict_types=1);

namespace App\Helper;

class BasketHelper
{
    private array $books;

    public function __construct(array $books = [])
    {
        $this->books = $books;
    }

    public function getTotalPrice(): float
    {
        return array_reduce($this->books, function ($carry, $book) {
            return $carry + $book['price'];
        });
    }

    public function getRawPrice(): int
    {
        return (int) ($this->getTotalPrice() * 100);
    }

    public function getDescription(): string
    {
        return implode(', ', array_map(function ($book) {
            return $book['title'];
        }, $this->books));
    }
}

<?php

namespace App\Controller;

use App\Helper\BookHelper;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends BaseController
{
    /**
     * @Route("/", name="home", methods={"GET"})
     */
    public function index(): Response
    {
        $books = BookHelper::createBooks();

        return $this->render('home/index.html.twig', compact('books'));
    }
}

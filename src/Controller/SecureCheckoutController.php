<?php

namespace App\Controller;

use App\Helper\BasketHelper;
use Stripe\PaymentIntent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SecureCheckoutController extends BaseCheckoutController
{
    /**
     * @Route("/secure/checkout", name="secure-checkout", methods={"POST"})
     *
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function index(Request $request): Response
    {
        $basket = new BasketHelper($this->getBasket());

        if ('submit_payment' == $request->get('action')) {
            return $this->redirectToRoute('success');
        }

        $intent = PaymentIntent::create([
            'amount' => $basket->getRawPrice(),
            'currency' => 'usd',
            'description' => $basket->getDescription(),
        ]);

        return $this->render('secure_checkout/index.html.twig', [
            'clientSecret' => $intent->client_secret,
        ]);
    }
}

<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BasketController extends BaseController
{
    /**
     * @Route("/basket", name="basket")
     */
    public function view(Request $request): Response
    {
        if ($request->isMethod('POST')) {
            $this->resetBasket();
        }

        $basket = $this->getBasket();
        $totalPrice = array_reduce($basket, function ($carry, $book) {
            return $carry + $book['price'];
        });

        return $this->render('basket/view.html.twig', compact('basket', 'totalPrice'));
    }
}

<?php

namespace App\Controller;

use Stripe\Stripe;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

abstract class BaseCheckoutController extends BaseController
{
    public function __construct(SessionInterface $session)
    {
        parent::__construct($session);

        Stripe::setApiKey($_ENV['STRIPE_PRIVATE_KEY']);
    }
}

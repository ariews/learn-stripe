<?php

namespace App\Controller;

use App\Helper\BasketHelper;
use Stripe\Charge;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LocalCheckoutController extends BaseCheckoutController
{
    /**
     * @Route("/local/checkout", name="local-checkout", methods={"POST"})
     *
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function index(Request $request): Response
    {
        if ('submit_payment' == $request->get('action')) {
            $token = $request->get('token');
            $basket = new BasketHelper($this->getBasket());

            $charge = Charge::create([
                'amount' => $basket->getRawPrice(),
                'currency' => 'usd',
                'description' => $basket->getDescription(),
                'source' => $token,
            ]);

            return $this->redirectToRoute('success');
        }

        return $this->render('local_checkout/index.html.twig', [
            'controller_name' => 'LocalCheckoutController',
        ]);
    }
}

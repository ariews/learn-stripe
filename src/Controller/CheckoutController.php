<?php

namespace App\Controller;

use Stripe\Checkout\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CheckoutController extends BaseCheckoutController
{
    /**
     * @Route("/checkout", name="checkout", methods={"POST"})
     *
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function index(): Response
    {
        $stripeSessionData = [
            'payment_method_types' => ['card'],
            'line_items' => [],
            'success_url' => 'http://localhost:8001/success?session_id={CHECKOUT_SESSION_ID}',
            'cancel_url' => 'http://localhost:8001/basket',
        ];

        foreach ($this->getBasket() as $item) {
            $stripeSessionData['line_items'][] = [
                'name' => $item['title'],
                'description' => $item['description'],
                'amount' => ($item['price'] * 100),
                'currency' => 'usd',
                'quantity' => 1,
            ];
        }

        $stripeSession = Session::create($stripeSessionData);

        return $this->render('checkout/index.html.twig', [
            'sessionId' => $stripeSession->id,
        ]);
    }
}

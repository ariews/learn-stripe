<?php

namespace App\Controller;

use App\Helper\BookHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends BaseController
{
    /**
     * @Route("/basket/add", name="ajax_basket_add", methods={"POST"})
     */
    public function add(Request $request): Response
    {
        $json = json_decode($request->getContent());
        $basket = $this->getBasket();

        if ($book = BookHelper::findById($json->id)) {
            $basket[] = $book->jsonSerialize();
        }

        $this->session->set('basket', $basket);

        return $this->json($basket);
    }

    /**
     * @Route("/basket/list", name="ajax_basket_list", methods={"GET"})
     */
    public function list(): Response
    {
        return $this->json($this->getBasket());
    }
}

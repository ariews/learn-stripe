<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SuccessController extends BaseController
{
    /**
     * @Route("/success", name="success", methods={"GET", "POST"})
     */
    public function index(): Response
    {
        $this->resetBasket();

        return $this->render('success/index.html.twig', [
            'controller_name' => 'SuccessController',
        ]);
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

abstract class BaseController extends AbstractController
{
    protected SessionInterface $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    protected function resetBasket(): void
    {
        $this->session->set('basket', []);
    }

    protected function getBasket(): array
    {
        return $this->session->get('basket', []);
    }
}

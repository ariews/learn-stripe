<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PaymentNotificationController extends AbstractController
{
    /**
     * @var null|object|\Symfony\Component\HttpKernel\Log\Logger
     */
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/payment/notification", name="payment_notification")
     */
    public function index(Request $request): Response
    {
        $this->logger->debug('CALLED', $request->toArray());

        return $this->json([
            'status' => 'OK',
            'code' => 200,
        ]);
    }
}

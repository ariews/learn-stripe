<?php

return (new PhpCsFixer\Config())
  ->setIndent('    ')
	->setRiskyAllowed(true)
  ->setRules([
		'@PSR2' => true,
		'@Symfony' => true,
		'@PhpCsFixer' => true,
		'@PHP54Migration' => true,
		'@PHP70Migration' => true,
		'@PHP71Migration' => true,
		'@PHP73Migration' => true,
		'@PHP74Migration' => true,
		'align_multiline_comment' => true,
		'comment_to_phpdoc' => ['ignored_tags'=>['todo']],
		'control_structure_continuation_position' => true,
		'declare_parentheses' => true,
		'global_namespace_import' => true,
		'not_operator_with_successor_space' => true,
		'nullable_type_declaration_for_default_null_value' => true,
		'phpdoc_line_span' => true,
		'phpdoc_tag_casing' => true,
		'simplified_null_return' => true,
		'single_line_comment_style' => false,
  ])
  ->setFinder(
    PhpCsFixer\Finder::create()
      ->exclude(['vendor', 'var'])
      ->in(__DIR__)
  )
;
